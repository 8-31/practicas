document.getElementById("btnEnviar").addEventListener('click', function () {
    const id = document.getElementById("txtId").value;

    try {
        const parsedId = parseInt(id);

        if (!id || isNaN(parsedId) || parsedId < 1 || parsedId > 100) {
            throw new Error("Ingrese un ID válido (número entre 1 y 100).");
        }

        const http = new XMLHttpRequest();
        const url = "https://jsonplaceholder.typicode.com/albums/" + parsedId;

        // Realizar función de respuesta de petición
        http.onreadystatechange = function () {
            // Validar respuesta
            if (this.status == 200 && this.readyState == 4) {
                let res = document.getElementById('lista');
                res.innerHTML = ""; // Limpiar la lista antes de cargar nuevos datos

                try {
                    const json = JSON.parse(this.responseText);

                    if (json && json.title) {
                        // Assuming json is an object and has a "title" property
                        const title = json.title;
                        res.innerHTML += '<tr><td class="columna3">' + title + '</td></tr>';

                        // Mostrar la tabla
                        document.getElementById('tabla').style.display = 'table';
                    } else {
                        throw new Error("La respuesta no contiene el formato esperado.");
                    }
                } catch (error) {
                    console.error("Error parsing JSON:", error);
                    alert("Hubo un problema al procesar la respuesta del servidor.");
                }
            }
        };

        http.open("GET", url, true);
        http.send();

    } catch (error) {
        alert(error.message);
    }
});
