// index.js

document.addEventListener("DOMContentLoaded", function () {
    const btnLimpiar = document.getElementById("btnLimpiar");
    const btnCargar = document.getElementById("btnCargar");
    const tabla = document.getElementById("tabla");
    const lista = document.getElementById("lista");

    btnLimpiar.addEventListener("click", function () {
        lista.innerHTML = ""; // Clear the table body
    });

    btnCargar.addEventListener("click", function () {
        const xhr = new XMLHttpRequest();
        const apiUrl = "https://jsonplaceholder.typicode.com/users";

        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    const data = JSON.parse(xhr.responseText);
                    // Iterate through each user and create a table row for each
                    let userRows = "";
                    data.forEach(user => {
                        userRows += `<tr>
                                        <td>${user.id}</td>
                                        <td>${user.name}</td>
                                        <td>${user.username}</td>
                                        <td>${user.email}</td>
                                        <td>${user.address.street}, ${user.address.suite}, ${user.address.city}, ${user.address.zipcode}</td>
                                        <td>${user.phone}</td>
                                        <td>${user.website}</td>
                                        <td>${user.company.name}</td>
                                        <td>${user.company.catchPhrase}</td>
                                        <td>${user.company.bs}</td>
                                    </tr>`;
                    });

                    lista.innerHTML = userRows;
                } else {
                    console.error("Error fetching data. Status:", xhr.status);
                }
            }
        };

        xhr.open("GET", apiUrl, true);
        xhr.send();
    });
});
