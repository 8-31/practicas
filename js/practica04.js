// practica04.js

document.addEventListener("DOMContentLoaded", function () {
    const btnCargar = document.getElementById("btnCargar");
    const userIdInput = document.getElementById("userId");
    const tabla = document.getElementById("tabla");
    const lista = document.getElementById("lista");

    btnCargar.addEventListener("click", function () {
        const userId = parseInt(userIdInput.value);

        if (isNaN(userId) || userId < 1 || userId > 10) {
            alert("Ingrese un ID de usuario válido (entre 1 y 10).");
            return;
        }

        const xhr = new XMLHttpRequest();
        const apiUrl = `https://jsonplaceholder.typicode.com/users/${userId}`;

        xhr.onreadystatechange = function () {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                if (xhr.status === 200) {
                    const user = JSON.parse(xhr.responseText);

                    const userRow = `<tr>
                                        <td>${user.id}</td>
                                        <td>${user.name}</td>
                                        <td>${user.username}</td>
                                        <td>${user.email}</td>
                                        <td>${user.address.street}, ${user.address.suite}, ${user.address.city}, ${user.address.zipcode}</td>
                                        <td>${user.phone}</td>
                                        <td>${user.website}</td>
                                        <td>${user.company.name}</td>
                                        <td>${user.company.catchPhrase}</td>
                                        <td>${user.company.bs}</td>
                                    </tr>`;

                    lista.innerHTML = userRow;
                } else {
                    alert("Error al cargar la información del usuario. Por favor, intente nuevamente.");
                }
            }
        };

        xhr.open("GET", apiUrl, true);
        xhr.send();
    });//AA
});
