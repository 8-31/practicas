function cargarDatos() {
    const http = new XMLHttpRequest();
    const url = "https://jsonplaceholder.typicode.com/albums";

    // Realizar función de respuesta de petición
    http.onreadystatechange = function () {
        // Validar respuesta
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById('lista');
            const json = JSON.parse(this.responseText);

            for (const datos of json) {
                res.innerHTML += '<tr><td class="columna1">' + datos.userId + '</td>' +
                    '<td class="columna2">' + datos.id + '</td>' +
                    '<td class="columna3">' + datos.title + '</td></tr>';
            }
        }
    };

    http.open("GET", url, true);
    http.send();
}

document.getElementById("btnCargar").addEventListener('click', function () {
    try {
        const numeroElementos = parseInt(prompt("Ingrese la cantidad de elementos a cargar (1-100):"));

        if (isNaN(numeroElementos) || numeroElementos < 1 || numeroElementos > 100) {
            throw new Error("Por favor, ingrese un número válido entre 1 y 100.");
        }

        cargarDatos();
    } catch (error) {
        alert(error.message);
    }
});

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});
